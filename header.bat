@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "tmpFile=%TEMP%\~horoscope.py"

:: Variables
SET "pipR=pip install Pillow"
SET "py=python.exe"

:: Check python installation
!py! --version >NUL

:: Clean potential error or python version
CLS

IF ERRORLEVEL 1 (
  ECHO Installation of Python not found, installation...

  :: Accept Winget ToT
  ECHO Y | winget list >NUL

  :: Install Python 3.12 from MS Store
  ECHO Y | winget install -he 9NCVDN91XZQP

  ECHO Download and installation of dependencies...

  :: Location of Python
  SET "py=%LOCALAPPDATA%\Microsoft\WindowsApps\!py!"

  :: Update pip
  !py! -m pip install --upgrade pip

  :: Install dependencies
  !py! -m !pipR!
) ELSE (
  :: Check dependencies
  !py! -m !pipR! >NUL
)

:: Write the program
CALL :getLine "::python_beg" "::python_end" > "!tmpFile!"

:: Run the program
!py! "!tmpFile!" online
EXIT /B

:getLine <beg str> <end str>
  SET "bBegEnd=0"
  FOR /F "usebackq delims=" %%i IN ("%~f0") do (
    IF !bBegEnd! EQU 1 (
      IF "%%i" EQU "%~2" ( EXIT /B )
      SETLOCAL DISABLEDELAYEDEXPANSION
      ECHO %%i
      ENDLOCAL
    ) ELSE (
      IF "%%i" EQU "%~1" ( SET "bBegEnd=1" )
    )
  )

ENDLOCAL
EXIT /B
