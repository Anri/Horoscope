HEADER = header.bat
PYTHON = main.py

OUTPUT = horoscope.bat

all: build

build:
	cat $(HEADER) > $(OUTPUT)
	echo "" >> $(OUTPUT)
	echo "::python_beg" >> $(OUTPUT)
	cat $(PYTHON) >> $(OUTPUT)
	echo "" >> $(OUTPUT)
	echo "::python_end" >> $(OUTPUT)
